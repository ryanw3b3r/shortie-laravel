import '../css/app.css'
import Alpine from 'alpinejs'

Alpine.data('main', () => ({
    codeValidated: true,
    shortLinkCopied: false,
    urlValidated: false,
    step: 1,
    response: '',
    error: false,
    shortcode: '',
    url: '',
    showOptions: false,
    expiry: 'Forever',
    expiryOptions: {
        'Forever': 'forever',
        'H1': '1 hour',
        'H3': '3 hours',
        'H6': '6 hours',
        'H12': '12 hours',
        'D1': '1 day',
        'D3': '3 days',
        'W1': '1 week',
        'W2': '2 weeks',
        'W4': '4 weeks',
        'M3': '3 months',
        'M6': '6 months',
        'Y1': '1 year',
    },
    isLoading: false,
    defaults() {
        this.codeValidated = true;
        this.shortLinkCopied = false;
        this.urlValidated = false;
        this.step = 1;
        this.response = '';
        this.error = false;
        this.shortcode = '';
        this.url = '';
        this.isLoading = false;
        this.$nextTick(() => this.$refs.url.focus());
    },
    async call(endpoint, payload) {
        return await fetch(endpoint, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload),
        });
    },
    selectIt() {
        setTimeout(() => {
            const selection = window.getSelection();
            const range = document.createRange();
            range.selectNodeContents(this.$refs.shortLinkEl);
            selection.removeAllRanges();
            selection.addRange(range);
            navigator.clipboard.writeText(this.$refs.shortLinkEl.innerText).then(() => {
                this.shortLinkCopied = true;
            });
            this.scrollDown();
        }, 600);
    },
    scroll(where = document.body.scrollHeight) {
        window.scrollTo({top: where, behavior: 'smooth'});
    },
    scrollDown() {
        this.scroll();
    },
    scrollUp() {
        this.scroll(-2000);
        this.$refs.clearButton.focus();
    },
    countOccurancesOf(sentence, word) {
        return sentence.split(word).length - 1;
    },
    async validate() {
        if (this.step === 1) {
            if (!this.url.startsWith('http')) {
                this.url = 'https://' + this.url;
            } else if (this.countOccurancesOf(this.url, 'https://') > 1) {
                this.url = this.url.substr(8);
            } else if (this.countOccurancesOf(this.url, 'http://') > 1) {
                this.url = this.url.substr(7);
            }
            this.urlValidated = this.url.includes('localhost') ||
                (this.url.includes('.') && this.url.substr(-1) !== '.');
        } else if (this.step === 2) {
            this.error = false;
            if (!this.shortcode) {
                this.codeValidated = true;
                return;
            }
            try {
                if (this.shortcode.length > 10) {
                    throw new Error('Shortcode is too long (max 10 chars).');
                }
                const response = await this.call(
                    '/check',
                    { code: this.shortcode }
                );
                if (!response.ok) {
                    throw new Error('Shortcode already taken!');
                }
                this.codeValidated = true;
            } catch(e) {
                this.error = e.message.trim();
            }
        }
    },
    async submitStepOne() {
        this.step++;
        setTimeout(() => {
            this.$refs.shortcode.focus();
            this.scrollDown();
        }, 900)
    },
    async submitStepTwo() {
        if (!this.codeValidated) {
            await this.validate();
            if (!this.codeValidated) {
                return false;
            }
        }
        this.step++;
        setTimeout(() => {
            this.scrollDown();
            this.$refs.expiry.focus();
        }, 600);
    },
    async submitStepThree() {
        this.isLoading = true;
        try {
            const response = await this.call(
                '/create',
                {
                    url: this.url,
                    code: this.shortcode,
                    expiry: this.expiry,
                    service: '2dots'
                }
            );
            if (!response.ok) {
                throw new Error('Server responded with an error!');
            }
            this.response = await response.json();
            this.isLoading = false;
            this.step++;
            this.selectIt();
        } catch(e) {
            this.isLoading = false;
            this.error = e.message.trim();
        }
    },
}))

Alpine.start();
