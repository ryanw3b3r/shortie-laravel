@props(['options', 'id' => (string) \Illuminate\Support\Str::uuid()])
<div>
    <div class="mt-1" x-on:click.outside="showOptions = false">
        <button type="button"
                @if($attributes->has('x-on:click'))
                    x-on:click="showOptions = !showOptions; {{ $attributes->get('x-on:click') }}"
                @else
                    x-on:click="showOptions = !showOptions"
                @endif
                {{ $attributes->whereDoesntStartWith('x-on:click') }}
                aria-haspopup="listbox"
                aria-expanded="true"
                aria-labelledby="{{ $id }}"
                class="relative w-full px-4 py-4 text-left transition-all duration-700 border outline-none min-w-64 bg-zinc-200 border-zinc-400 rounded-2xl text-zinc-700 disabled:cursor-not-allowed disabled:opacity-50 focus:bg-zinc-100 focus:text-zinc-900 focus:border-sky-600 focus:outline-none">
            <span class="block truncate" x-text="expiryOptions[expiry]"></span>
            <span class="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                <svg class="w-5 h-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M10 3a1 1 0 01.707.293l3 3a1 1 0 01-1.414 1.414L10 5.414 7.707 7.707a1 1 0 01-1.414-1.414l3-3A1 1 0 0110 3zm-3.707 9.293a1 1 0 011.414 0L10 14.586l2.293-2.293a1 1 0 011.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
                          clip-rule="evenodd" />
                </svg>
            </span>
        </button>
        <ul x-show="showOptions"
            tabindex="-1"
            role="listbox"
            aria-labelledby="{{ $id }}"
            class="w-full py-1 mt-1 overflow-auto text-base shadow-sm bg-zinc-200 max-h-60 rounded-2xl ring-1 ring-sky-600 focus:outline-none focus:bg-zinc-100 hover:bg-zinc-100"
            x-transition.opacity.duration.600ms>
            @foreach($options as $option)
                <li class="relative py-2 pl-3 cursor-default select-none text-zinc-900 pr-9 group focus:bg-sky-600 focus:text-white hover:bg-sky-600 hover:text-white"
                    x-bind:class="expiry === '{{ $option->name }}' ? 'bg-sky-600 text-white' : ''"
                    x-on:click="expiry = '{{ $option->name }}'; showOptions = false"
                    id="listbox-option-{{ $option->name }}"
                    role="option">
                    <span class="block truncate"
                          x-bind:class="expiry === '{{ $option->name }}' ? 'font-semibold' : 'font-normal'">{{ $option->value }}</span>
                    <span x-show="expiry === '{{ $option->name }}'"
                          class="absolute inset-y-0 right-0 flex items-center pr-4 group-focus:text-white group-hover:text-white"
                          x-bind:class="expiry === '{{ $option->name }}' ? 'text-white' : 'text-sky-600'">
                        <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                clip-rule="evenodd" />
                        </svg>
                    </span>
                </li>
            @endforeach
        </ul>
    </div>
</div>
