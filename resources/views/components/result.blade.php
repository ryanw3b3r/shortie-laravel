<div x-show="response"
     class="max-w-full p-4 mt-8 overflow-auto border rounded-xl bg-zinc-50 border-zinc-400 text-zinc-900"
     x-transition>
    <p class="mb-4"><strong>Original URL:</strong> <span x-text="response.url" class="break-words"></span><p>
    <p><strong>Short version:</strong> <span x-on:click="selectIt" x-text="response.fullUrl" x-ref="shortLinkEl"></span><p>
</div>
<div x-show="shortLinkCopied" class="my-4 font-semibold text-sky-600">Short URL has been copied to your clipboard!</div>
<div x-show="shortLinkCopied" class="my-8">Thank you for using our services!</div>
