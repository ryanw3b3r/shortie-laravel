<button type="button"
        x-on:click="scrollUp()"
        class="absolute w-8 h-8 transition-all duration-700 bg-white border rounded-full -top-4 right-4 border-sky-600 group hover:w-24 focus:w-24">
    <span class="relative flex items-center justify-end px-1">
        <span class="absolute text-white transition-all duration-700 scale-x-0 whitespace-nowrap left-2 group-hover:text-sky-600 group-hover:scale-x-100 group-hover:px-2 group-focus:text-sky-600 group-focus:scale-x-100 group-focus:px-2">scroll up</span>
        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-sky-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 11l7-7 7 7M5 19l7-7 7 7" />
        </svg>
    </span>
</button>
