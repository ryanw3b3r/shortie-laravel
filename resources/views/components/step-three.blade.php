<div x-show="step > 2"
     class="flex flex-col items-center w-full"
     x-transition.duration.1000ms>
    <x-heading>Would you like the link to expire or should it be active forever?</x-heading>
    <form @submit.prevent="submitStepThree()"
          class="flex flex-col items-start px-4 space-y-8 md:flex-row md:space-y-0 md:space-x-4"
          x-transition>
        <x-select x-model="expiry"
                  x-bind:disabled="isLoading || step !== 3 || error"
                  x-ref="expiry"
                  x-on:click="setTimeout(() => scrollDown(), 200)"
                  :options="\App\Models\ExpiryOptions::cases()"/>
        <x-button x-bind:disabled="step !== 3">Generate!</x-button>
    </form>
</div>
