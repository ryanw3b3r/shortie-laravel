<form @submit.prevent="defaults()" {{ $attributes }}>
    <button type="submit"
            x-ref="clearButton"
            class="flex items-center justify-center px-8 py-1 transition-all duration-700 bg-white border outline-none min-w-64 whitespace-nowrap rounded-2xl border-sky-800 border-opacity-20 text-zinc-800 focus:border-sky-900 focus:bg-zinc-100 focus:outline-none">
        <svg xmlns="http://www.w3.org/2000/svg"
             class="w-6 h-6 mr-2"
             fill="none"
             viewBox="0 0 24 24"
             stroke="currentColor">
            <path stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15" />
        </svg>
        <span>Start again</span>
    </button>
</form>
