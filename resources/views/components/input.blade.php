@props(['id' => (string) \Illuminate\Support\Str::uuid()])
<input id="{{ $id }}" name="{{ $id }}" {{ $attributes->merge([
    'class' => 'md:w-96 bg-zinc-200 border border-zinc-400 rounded-2xl px-4 py-4 text-zinc-700 outline-none transition-all ' .
               'duration-700 w-full placeholder-zinc-400 focus:placeholder-zinc-300 focus:bg-zinc-100 focus:text-zinc-900 ' .
               'focus:border-sky-600 focus:outline-none disabled:cursor-not-allowed disabled:opacity-50'
]) }}/>
