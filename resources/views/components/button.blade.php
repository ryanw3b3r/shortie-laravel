@props(['type' => 'submit'])
<button type="{{ $type }}" {{ $attributes->merge([
    'class' => 'whitespace-nowrap px-8 py-4 rounded-2xl bg-sky-600 border ' .
               'border-sky-800 text-zinc-100 outline-none transition-all ' .
               'duration-700 focus:border-sky-900 focus:bg-sky-700 ' .
               'focus:outline-none md:hover:scale-105 md:disabled:hover:scale-100 ' .
               'disabled:cursor-not-allowed disabled:opacity-50'
]) }}>{{ $slot }}</button>
