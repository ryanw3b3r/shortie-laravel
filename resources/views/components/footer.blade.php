<footer class="relative w-full px-4 py-2 mt-4 text-xs text-center bg-white border-t text-zinc-500 border-sky-600">
    Copyright &copy; 2021 - {{ date('Y') }}
    <a href="https://toodots.com"
    title="TooDots website"
    class="underline underline-offset-4">TooDots Ltd</a>. All rights reserved.
    <x-scroll-up-button/>
</footer>
