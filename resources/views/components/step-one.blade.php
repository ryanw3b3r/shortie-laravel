<x-heading>Make any long link shorter. Just paste it into the field below and click on a few button. That's all!</x-heading>
<form @submit.prevent="submitStepOne()"
      class="flex flex-col items-center w-full px-4 space-y-8 md:flex-row md:space-y-0 md:space-x-4">
    <x-input type="url"
             placeholder="https://"
             value="https://"
             x-ref="url"
             x-model="url"
             x-bind:disabled="isLoading || step !== 1"
             autofocus
             @keyup="validate"
             class="grow"/>
    <x-button x-bind:disabled="isLoading || error || !urlValidated || step !== 1">Next &raquo;</x-button>
</form>
