<div x-show="step > 1"
     class="flex flex-col items-center w-full"
     x-transition.duration.1000ms>
    <x-heading>Would you like to use your own shortcode? If so, then please type it in...</x-heading>
    <form @submit.prevent="submitStepTwo()"
          class="flex flex-col items-center px-4 space-y-8 md:flex-row md:space-y-0 md:space-x-4"
          x-transition>
        <x-input type="text"
                 x-model="shortcode"
                 x-bind:disabled="isLoading || step !== 2"
                 @keyup.debounce.500ms="validate"
                 x-ref="shortcode"/>
        <x-button x-bind:disabled="!codeValidated || step !== 2 || error">Next &raquo;</x-button>
    </form>
</div>
