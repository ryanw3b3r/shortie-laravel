<p class="mt-4 text-red-800" x-show="error" x-transition>
    <strong>Error! </strong>
    <span x-text="error"></span><span x-show="error && !['.', ',', '!'].includes(error.slice(-1))">.</span> Please try again.
</p>
