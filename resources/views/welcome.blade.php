<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="min-h-screen scroll-smooth">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>2dots.info</title>
        <link rel="icon" type="image/svg+xml" href="{{ asset('assets/logo.svg') }}">
        <link rel="alternate icon" href="{{ asset('favicon.ico') }}">
        {!! \App\Helpers\Vite::assets() !!}
    </head>
    <body class="min-h-screen antialiased bg-concrete text-zinc-900">
        <div class="flex flex-col min-h-screen mx-auto" x-data="main">
            <div class="max-w-screen-md mx-auto grow">
                <div class="flex flex-col items-center" x-cloak>
                    <x-logo class="mt-8 fill-current w-36 text-zinc-600"/>
                    <x-start-over class="mt-12"/>
                    <div class="flex flex-col items-center w-full">
                        <x-step-one/>
                        <x-step-two/>
                        <x-step-three/>
                    </div>
                    <x-errors/>
                    <x-result/>
                </div>
            </div>
            <x-footer/>
        </div>
    </body>
</html>
