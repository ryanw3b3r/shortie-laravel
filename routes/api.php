<?php

use App\Http\Controllers\CodeChecker;
use App\Http\Controllers\CodeCreator;
use App\Http\Controllers\CodeRedirector;
use App\Http\Middleware\LogGetRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;

Route::get('/{code}', CodeRedirector::class)->middleware(LogGetRequest::class);
Route::post('/create', CodeCreator::class);
Route::post('/check', CodeChecker::class);

Route::fallback(
    fn() => response()->json(
        ['error' => 'Route not found'],
        Response::HTTP_NOT_FOUND
    )
);
