<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCodesAddExpiryColumn extends Migration
{
    public function up(): void
    {
        Schema::table(
            'codes',
            fn (Blueprint $table) => $table->string('expiry', 9)->default('Forever')
        );
    }

    public function down(): void
    {
        Schema::table(
            'codes',
            fn (Blueprint $table) => $table->dropColumn('expiry')
        );
    }
}
