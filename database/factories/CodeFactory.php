<?php

namespace Database\Factories;

use App\Models\ExpiryOptions;
use Illuminate\Database\Eloquent\Factories\Factory;

class CodeFactory extends Factory
{
    public function definition(): array
    {
        $expiryOptions = array_column(ExpiryOptions::cases(), 'name');

        return [
            'id' => $this->faker->randomLetter() .
                $this->faker->randomLetter() .
                $this->faker->randomLetter() .
                $this->faker->randomLetter(),
            'url' => $this->faker->url,
            'expiry' => $expiryOptions[rand(0, count($expiryOptions) - 1)],
            'service' => $this->faker->text(20),
        ];
    }
}
