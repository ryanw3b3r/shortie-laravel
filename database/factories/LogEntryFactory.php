<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class LogEntryFactory extends Factory
{
    public function definition(): array
    {
        return [
            'ip' => $this->faker->ipv4(),
            'referrer' => $this->faker->url(),
            'code' => $this->faker->randomLetter() .
                $this->faker->randomLetter(),
        ];
    }
}
