<?php

namespace Database\Seeders;

use App\Models\Code;
use App\Models\LogEntry;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        Code::factory(10)
            ->create()
            ->each(
                fn ($code) => LogEntry::factory(rand(0, 5))
                    ->create(['code' => $code])
            );
    }
}
