<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Code extends Model
{
    use HasFactory;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'id',
        'url',
        'expiry',
        'service',
    ];

    protected $appends = ['fullUrl'];

    public function getFullUrlAttribute(): string
    {
        return trim(config('app.url'), '/') . '/' . $this->id;
    }

    public static function addNew(array $data, int $attempt = 0): self
    {
        if (isset($data['code'])) {
            $data['id'] = $data['code'];
        }

        if (!isset($data['id']) || $attempt) {
            $data['id'] = self::generateId(max(2, $attempt));
        }

        if ($attempt > 8) {
            throw new \Exception('Cannot generate unique url');
        }

        try {
            return self::create($data);
        } catch (QueryException $error) {
            $attempt && Log::error($error->getMessage(), $error->getTrace());
            return self::addNew($data, ++$attempt);
        }
    }

    public function scopeWithExpiryDates(Builder $query): Builder
    {
        return $query->whereRaw(
            'expiry <> "" AND expiry IS NOT NULL AND expiry <> "Forever"'
        );
    }

    protected static function generateId(int $length): string
    {
        return Str::lower(Str::random($length));
    }
}
