<?php

namespace App\Models;

enum ExpiryOptions: string
{
    case Forever = 'forever';
    case H1 = '1 hour';
    case H3 = '3 hours';
    case H6 = '6 hours';
    case H12 = '12 hours';
    case D1 = '1 day';
    case D3 = '3 days';
    case W1 = '1 week';
    case W2 = '2 weeks';
    case W4 = '4 weeks';
    case M3 = '3 months';
    case M6 = '6 months';
    case Y1 = '1 year';
}
