<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\HtmlString;

class Vite
{
    public static function assets(): HtmlString
    {
        $devServerIsRunning = false;
        $devServer = env('VITE_DEV_SERVER_URL');

        if (app()->environment('local')) {
            try {
                Http::withoutVerifying()->get($devServer);
                $devServerIsRunning = true;
            } catch (\Throwable) {}
        }

        if ($devServerIsRunning) {
            return new HtmlString(<<<HTML
                    <script type="module" src="{$devServer}/@vite/client" defer></script>
                    <script type="module" src="{$devServer}/resources/js/app.js" defer></script>
                HTML);
        }

        $manifest = json_decode(file_get_contents(public_path('build/manifest.json')), true);

        return new HtmlString(<<<HTML
                <script type="module" src="/build/{$manifest['resources/js/app.js']['file']}" defer></script>
                <link rel="stylesheet" href="/build/{$manifest['resources/js/app.js']['css'][0]}">
            HTML);
    }
}
