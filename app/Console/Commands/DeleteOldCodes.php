<?php

namespace App\Console\Commands;

use App\Models\Code;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class DeleteOldCodes extends Command
{
    protected $signature = 'codes:delete-old';
    protected $description = 'Delete old codes from database';

    public function handle()
    {
        try {
            $codesToDelete = [];
            $codesToExpire = Code::withExpiryDates()->get();

            foreach ($codesToExpire as $code) {
                $expiry = $code->expiry;
                $option = constant("\App\Models\ExpiryOptions::$expiry");
                if (Carbon::now()->sub($option->value) >= $code->created_at) {
                    $codesToDelete[] = $code->id;
                }
            }

            Code::whereIn('id', $codesToDelete)->delete();

            return Command::SUCCESS;
        } catch (\Throwable) {
            return Command::FAILURE;
        }
    }
}
