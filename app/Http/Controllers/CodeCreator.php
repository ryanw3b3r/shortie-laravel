<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCodeRequest;
use App\Models\Code;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class CodeCreator
{
    public function __invoke(CreateCodeRequest $request): JsonResponse
    {
        $code = Code::addNew($request->validated());

        if ($code) {
            return response()->json($code, Response::HTTP_OK);
        }

        return response()->json('[]', Response::HTTP_BAD_REQUEST);
    }
}
