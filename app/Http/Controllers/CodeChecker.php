<?php

namespace App\Http\Controllers;

use App\Models\Code;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class CodeChecker
{
    public function __invoke(Request $request): JsonResponse
    {
        $code = Code::find($request->post('code'));

        if ($code || Str::length($request->post('code')) > 10) {
            return response()->json('[]', Response::HTTP_NOT_ACCEPTABLE);
        }

        return response()->json('[]', Response::HTTP_OK);
    }
}
