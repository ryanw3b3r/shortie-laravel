<?php

namespace App\Http\Controllers;

use App\Models\Code;

class CodeRedirector
{
    public function __invoke(Code $code)
    {
        return redirect($code->url);
    }
}
