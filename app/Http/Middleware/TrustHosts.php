<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class TrustHosts
{
    protected array $trustedIps = [
        '127.0.0.1',
        '10.114.16.2',
        '188.95.144.143',
        '46.101.192.222',
    ];

    public function handle(Request $request, Closure $next)
    {
        if (in_array($request->getClientIp(), $this->trustedIps)) {
            return $next($request);
        }

        Log::error(
            'TrustHosts: blocked IP ' . $request->getClientIp(),
            $request->json()->all()
        );

        return response()->json(
            ['error' => 'No access'],
            Response::HTTP_FORBIDDEN
        );
    }
}
