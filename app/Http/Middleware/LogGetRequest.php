<?php

namespace App\Http\Middleware;

use App\Models\Code;
use App\Models\LogEntry;
use Closure;
use Illuminate\Http\Request;

class LogGetRequest
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->isMethod('GET') &&
            $request->segment(1) &&
            Code::find($request->segment(1))->exists()
        ) {
            LogEntry::create([
                'ip' => $request->getClientIp(),
                'referrer' => $request->header('referrer'),
                'code' => $request->segment(1),
            ]);
        }
        return $next($request);
    }
}
