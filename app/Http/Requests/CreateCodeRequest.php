<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCodeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'url' => ['required'],
            'code' => ['sometimes', 'max:10'],
            'expiry' => ['required', 'max:9'],
            'service' => ['required', 'max:20'],
        ];
    }
}
