# Shortie in Laravel (old 2DOTS.info)

Link generator services and notification channels.


## Basic usage

GET `https://2dots.info/<<shortcode>>` - to redirect to previously registered page

POST `https://2dots.info/create` - to create new shortcode

POST payload example: 

```php
[  
  'url' => 'url to redirect to',  
  'service' => 'service that requests to store this redirection',  
]
```


## Development

Use `npm run dev` to have a hot reload server.  
Use `npm run build` to build all assets for production.  
