<?php

namespace Tests\Feature;

use App\Models\Code;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CodeRedirectorTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function returns_redirect_on_valid_data()
    {
        $data = [
            'url' => 'https://toodots.com',
            'service' => '2dots.info',
        ];

        $code = Code::addNew($data);
        $shortCode = $code->id;

        $response = $this->get("/$shortCode");

        $response->assertStatus(302);
        $response->assertRedirect('https://toodots.com');
    }
}
