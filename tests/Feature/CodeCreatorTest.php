<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CodeCreatorTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function creator_will_generate_entry_for_valid_data()
    {
        $data = [
            'url' => 'https://toodots.com',
            'expiry' => 'D1',
            'service' => '2dots.info',
        ];

        $response = $this->postJson('/create', $data);

        $response->assertStatus(200);
        $this->assertDatabaseHas('codes', $data);

        $response->assertJson(
            fn (AssertableJson $json) => $json->hasAll(
                'id',
                'url',
                'service',
                'fullUrl',
                'expiry',
                'created_at',
                'updated_at',
            )
        );
    }

    /**
     * @test
     */
    public function creator_will_throw_error_for_invalid_data()
    {
        $data = ['url' => 'https://toodots.com'];
        $response = $this->postJson('/create', $data);

        $response->assertStatus(422);
        $this->assertDatabaseMissing('codes', $data);
    }
}
