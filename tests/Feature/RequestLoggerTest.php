<?php

namespace Tests\Feature;

use App\Models\Code;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RequestLoggerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function get_requests_are_logged_in_database()
    {
        $code = Code::factory()->create();

        $response = $this->get(
            '/' . $code->id,
            ['referrer' => 'https://example.com']
        );

        $response->assertStatus(302);
        $this->assertDatabaseHas(
            'log_entries',
            [
                'ip' => '127.0.0.1',
                'referrer' => 'https://example.com',
                'code' => $code->id,
            ]
        );
    }

    /**
     * @test
     */
    public function post_requests_are_not_logged_in_database()
    {
        Code::factory()->create();

        $response = $this->post(
            '/create',
            [
                'url' => 'https://example.com',
                'expiry' => 'D1',
                'service' => '2dots.info',
            ]
        );

        $response->assertStatus(200);
        $this->assertDatabaseMissing(
            'log_entries',
            [
                'ip' => '127.0.0.1',
                'referrer' => 'https://example.com',
            ]
        );
    }
}
