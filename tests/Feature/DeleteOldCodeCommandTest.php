<?php

namespace Tests\Feature;

use App\Console\Commands\DeleteOldCodes;
use App\Models\Code;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeleteOldCodeCommandTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function command_deletes_codes_that_expired()
    {
        Code::factory(1)
            ->create([
                'created_at' => now()->subMonths(3),
                'expiry' => 'Forever'
            ]);

        Code::factory()
            ->count(7)
            ->state(new Sequence(
                fn () => ['created_at' => now()->subWeeks(rand(2, 30)), 'expiry' => 'Y1'],
            ))
            ->create();

        Code::factory()
            ->count(3)
            ->state(new Sequence(
                fn () => ['created_at' => now()->subDays(rand(2, 30)), 'expiry' => 'D1'],
            ))
            ->create();
        
        $this->artisan(DeleteOldCodes::class)->assertSuccessful();
        $this->assertDatabaseCount('codes', 8);
    }
}
