<?php

namespace Tests\Feature;

use App\Models\Code;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CodeModelTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function same_url_will_have_different_codes()
    {
        $data = [
            'id' => 'xy',
            'url' => 'https://toodots.com',
            'service' => '2dots.info',
        ];

        $oldCode = Code::addNew($data);
        $oldCode->updated_at = now()->subDays(3);
        $oldCode->save();

        $newCode = Code::addNew($data);

        $this->assertNotEquals($oldCode->id, $newCode->id);
        $this->assertEquals($oldCode->url, $newCode->url);
        $this->assertDatabaseCount('codes', 2);
    }
}
